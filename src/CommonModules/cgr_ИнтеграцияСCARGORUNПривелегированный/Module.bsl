Процедура ПриЗаписиОбъектовСинхронизации(Источник, Отказ) Экспорт
	УстановитьПривилегированныйРежим(Истина);

	Если Источник.ДополнительныеСвойства.Свойство("ЗагрузкаИзCARGORUN") Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	cgr_ИдентификаторыОбъектовВСервисе.ТипОбъекта
		|ИЗ
		|	РегистрСведений.cgr_ИдентификаторыОбъектовВСервисе КАК cgr_ИдентификаторыОбъектовВСервисе
		|ГДЕ
		|	cgr_ИдентификаторыОбъектовВСервисе.Объект = &Объект";
	
	Запрос.УстановитьПараметр("Объект", Источник.Ссылка);
	
	мТипыОбъектовКУдалению =  Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("ТипОбъекта");
	мТипыОбъектаКДобавлению = cgr_ИнтеграцияСCARGORUNВызовСервера.ОпределитьТипПоСсылкеНаОбъект(Источник.Ссылка);
	мТипыОбъектаКОбновлению = Новый Массив();
	Для Каждого ТипОбъекта Из мТипыОбъектаКДобавлению Цикл
		НайденыйИндекс = мТипыОбъектовКУдалению.Найти(ТипОбъекта);
		Если НайденыйИндекс <> Неопределено Тогда
			мТипыОбъектовКУдалению.Удалить(НайденыйИндекс);
		КонецЕсли;
		мТипыОбъектаКОбновлению.Добавить(ТипОбъекта);
	КонецЦикла;
	Для Каждого ТипОбъекта Из мТипыОбъектовКУдалению Цикл
		мТипыОбъектаКОбновлению.Добавить(ТипОбъекта);
	КонецЦикла;

	ВыгружаемыеТипыОбъекта = Новый Массив;

	Для Каждого ТипОбъекта Из мТипыОбъектаКОбновлению Цикл
		Если cgr_ИнтеграцияСCARGORUN.ВыгружатьИз1ССразу(ТипОбъекта) Тогда
			СоответствиеИдентификаторовИОбъектов = Новый Соответствие;
			мСсылки = Новый Массив;
			мСсылки.Добавить(Источник.Ссылка);
			ТолькоУдаление = мТипыОбъектовКУдалению.Найти(ТипОбъекта) <> Неопределено;
			Попытка
				cgr_ИнтеграцияСCARGORUN.ВыгрузитьОбъектыВСервис(мСсылки, ТипОбъекта,
					СоответствиеИдентификаторовИОбъектов, ТолькоУдаление);
			Исключение
			КонецПопытки;
			Если Не ТолькоУдаление И СоответствиеИдентификаторовИОбъектов.Получить(Источник.Ссылка) = Неопределено Тогда
				ВыгружаемыеТипыОбъекта.Добавить(ТипОбъекта);
			КонецЕсли;
		ИначеЕсли cgr_ИнтеграцияСCARGORUN.ВыгружатьИз1С(ТипОбъекта) Тогда
			ВыгружаемыеТипыОбъекта.Добавить(ТипОбъекта);
		КонецЕсли;

	КонецЦикла;

	Если ВыгружаемыеТипыОбъекта.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	НомерЗадания = cgr_ИнтеграцияСCARGORUNСлужебный.ПолучитьНомерЗаданияВыгрузки();

	НачатьТранзакцию(РежимУправленияБлокировкойДанных.Управляемый);

	Попытка

		Блокировка = Новый БлокировкаДанных;
		ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.cgr_ОбъектыДляВыгрузкиВСервис");
		ЭлементБлокировки.Режим = РежимБлокировкиДанных.Разделяемый;
		ЭлементБлокировки.УстановитьЗначение("НомерЗадания", НомерЗадания);
		Блокировка.Заблокировать();

		Для Каждого ТипОбъекта Из ВыгружаемыеТипыОбъекта Цикл
			МенеджерЗаписи = РегистрыСведений.cgr_ОбъектыДляВыгрузкиВСервис.СоздатьМенеджерЗаписи();
			МенеджерЗаписи.НомерЗадания = НомерЗадания;
			МенеджерЗаписи.Объект = Источник.Ссылка;
			МенеджерЗаписи.ТипОбъекта = ТипОбъекта;
			МенеджерЗаписи.ТолькоУдаление = мТипыОбъектовКУдалению.Найти(ТипОбъекта) <> Неопределено;
			МенеджерЗаписи.Записать(Истина);
		КонецЦикла;

		ЗафиксироватьТранзакцию();

	Исключение

		ОтменитьТранзакцию();
		Отказ = Истина;
		ЗаписьЖурналаРегистрации("CARGORUN", УровеньЖурналаРегистрации.Ошибка, cgr_ИнтеграцияСCARGORUN, ,
			ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()), РежимТранзакцииЗаписиЖурналаРегистрации.Независимая);

	КонецПопытки;

КонецПроцедуры

Процедура ПолучитьФорматированныйТекстСостоянияОбменаАсинх(АдресХранилища, СсылкаНаОбъект) Экспорт
	УстановитьПривилегированныйРежим(Истина);

	Результат = Новый ФорматированнаяСтрока("Обмен с CARGO.RUN:");

	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	cgr_ЖурналСинхронизацииСрезПоследних.НаправлениеСинхронизации КАК НаправлениеСинхронизации,
	|	cgr_ЖурналСинхронизацииСрезПоследних.ТипОбъекта КАК ТипОбъекта,
	|	МАКСИМУМ(cgr_ЖурналСинхронизацииСрезПоследних.Период) КАК Период
	|ПОМЕСТИТЬ ВТ_Варианты
	|{ВЫБРАТЬ
	|	cgr_ЖурналСинхронизацииСрезПоследних.НаправлениеСинхронизации.*,
	|	cgr_ЖурналСинхронизацииСрезПоследних.ТипОбъекта.*,
	|	cgr_ЖурналСинхронизацииСрезПоследних.Объект.*,
	|	cgr_ЖурналСинхронизацииСрезПоследних.Идентификатор}
	|ИЗ
	|	РегистрСведений.cgr_ЖурналСинхронизации.СрезПоследних(, Объект = &СсылкаНаОбъект) КАК cgr_ЖурналСинхронизацииСрезПоследних
	|
	|СГРУППИРОВАТЬ ПО
	|	cgr_ЖурналСинхронизацииСрезПоследних.НаправлениеСинхронизации,
	|	cgr_ЖурналСинхронизацииСрезПоследних.ТипОбъекта
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	cgr_ЖурналСинхронизацииСрезПоследних.НаправлениеСинхронизации КАК НаправлениеСинхронизации,
	|	ПРЕДСТАВЛЕНИЕ(cgr_ЖурналСинхронизацииСрезПоследних.ТипОбъекта) КАК ТипОбъекта,
	|	cgr_ЖурналСинхронизацииСрезПоследних.Период КАК Период,
	|	cgr_ЖурналСинхронизацииСрезПоследних.ОписаниеОшибки КАК ОписаниеОшибки
	|ИЗ
	|	РегистрСведений.cgr_ЖурналСинхронизации.СрезПоследних(, Объект = &СсылкаНаОбъект) КАК cgr_ЖурналСинхронизацииСрезПоследних
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_Варианты КАК ВТ_Варианты
	|		ПО cgr_ЖурналСинхронизацииСрезПоследних.НаправлениеСинхронизации = ВТ_Варианты.НаправлениеСинхронизации
	|			И cgr_ЖурналСинхронизацииСрезПоследних.ТипОбъекта = ВТ_Варианты.ТипОбъекта
	|			И cgr_ЖурналСинхронизацииСрезПоследних.Период = ВТ_Варианты.Период";

	Запрос.УстановитьПараметр("СсылкаНаОбъект", СсылкаНаОбъект);

	РезультатЗапроса = Запрос.Выполнить();

	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();

	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Результат = Новый ФорматированнаяСтрока(Результат, Новый ФорматированнаяСтрока(Символы.ПС),
			Новый ФорматированнаяСтрока(?(ВыборкаДетальныеЗаписи.НаправлениеСинхронизации
			= Перечисления.cgr_НаправленияСинхронизации.ВыгрузкаИз1С, БиблиотекаКартинок.cgr_Upload,
			БиблиотекаКартинок.cgr_Download)), Новый ФорматированнаяСтрока("[" + ВыборкаДетальныеЗаписи.ТипОбъекта
			+ " от " + Формат(ВыборкаДетальныеЗаписи.Период, "ДФ='dd.MM.yyyy, HH:mm:ss'") + "] "));
		Если Не ПустаяСтрока(ВыборкаДетальныеЗаписи.ОписаниеОшибки) Тогда
			Результат = Новый ФорматированнаяСтрока(Результат,
				Новый ФорматированнаяСтрока(ВыборкаДетальныеЗаписи.ОписаниеОшибки, , Новый Цвет(255, 0, 0)));
		Иначе
			Результат = Новый ФорматированнаяСтрока(Результат, Новый ФорматированнаяСтрока("Успешно", , Новый Цвет(255,
				255, 255), Новый Цвет(40, 167, 69)));
		КонецЕсли;
	КонецЦикла;

	ПоместитьВоВременноеХранилище(Новый Структура("Выполнено, Результат", Истина, Результат), АдресХранилища);

КонецПроцедуры

Функция ОткрыватьВСервисе(ТипОбъекта) Экспорт

	Если Не ПравоДоступа("Просмотр", Метаданные.ОбщиеКоманды.cgr_ОткрытьОбъектВСервисеCARGORUN) Тогда
		Возврат Ложь;
	Иначе
		УстановитьПривилегированныйРежим(Истина);
		Результат = cgr_ИнтеграцияСCARGORUNПовтИсп.ОткрыватьВСервисе(ТипОбъекта);
		Возврат Результат;
	КонецЕсли;

КонецФункции

Функция ПолучитьURLДляОтображенияОбъекта(СсылкаНаОбъект, ТипОбъекта = Неопределено) Экспорт
	УстановитьПривилегированныйРежим(Истина);

	Результат = Новый Массив;

	мТипыОбъекта = Новый Массив;
	Если ТипОбъекта = Неопределено Тогда
		мТипыОбъекта = ОпределитьТипПоСсылкеНаОбъект(СсылкаНаОбъект);
	Иначе
		мТипыОбъекта.Добавить(ТипОбъекта);
	КонецЕсли;

	НастройкиПодключения = cgr_ИнтеграцияСCARGORUNПовтИсп.ПолучитьНастройкиПодключения();
	Если НастройкиПодключения = Неопределено Тогда
		Возврат Результат;
	КонецЕсли;

	ПрефиксURL = ?(НастройкиПодключения.ЗащищенноеСоединение, "https://", "http://") + НастройкиПодключения.Адрес + ?(
		НастройкиПодключения.Порт <> 80, ":" + Формат(НастройкиПодключения.Порт, "ЧН=0; ЧГ=0"), "");

	Для Каждого _ТипОбъекта Из мТипыОбъекта Цикл

		Идентификатор = 0;
		ШаблонURL = "";

		Если _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.Грузовик Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.Грузовик, СсылкаНаОбъект, , Истина);
			ШаблонURL = "/trucks/list/%1";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.Прицеп Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.Прицеп, СсылкаНаОбъект, , Истина);
			ШаблонURL = "/trailers/list/%1";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.Водитель Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.Водитель, СсылкаНаОбъект, , Истина);
			ШаблонURL = "/drivers/list/%1";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.Контрагент Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.Контрагент, СсылкаНаОбъект, , Истина);
			ШаблонURL = "/cargoOwners/list/%1";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.Заявка Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.Заявка, СсылкаНаОбъект, , Истина);
			ШаблонURL = "/bids/bid/%1";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.СтавкаНДС Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.СтавкаНДС, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.МаркаГрузовика Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.МаркаГрузовика, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ТипГрузовика Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ТипГрузовика, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.МаркаПрицепа Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.МаркаПрицепа, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ТипПрицепа Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ТипПрицепа, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ТипЗагрузки Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ТипЗагрузки, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ТипВыгрузки Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ТипВыгрузки, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ТипГруза Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ТипГруза, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ФормаОплаты Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ФормаОплаты, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		ИначеЕсли _ТипОбъекта = Перечисления.cgr_ТипыОбъектов.ТипПроизвольнойТочки Тогда
			Идентификатор = cgr_ИнтеграцияСCARGORUN.ПолучитьИдентификаторПоОбъекту(
				Перечисления.cgr_ТипыОбъектов.ФормаОплаты, СсылкаНаОбъект, , Истина);
			ШаблонURL = "";
		КонецЕсли;

		Если Идентификатор = 0 Или ПустаяСтрока(ШаблонURL) Тогда
			Продолжить;
		Иначе
			ПолныйURL = ПрефиксURL + СтрШаблон(ШаблонURL, Формат(Идентификатор, "ЧН=0; ЧГ=0"));
			Результат.Добавить(ПолныйURL);
		КонецЕсли;

	КонецЦикла;

	Возврат Результат;

КонецФункции

Функция ОпределитьТипПоСсылкеНаОбъект(СсылкаНаОбъект) Экспорт
	УстановитьПривилегированныйРежим(Истина);

	Возврат cgr_ИнтеграцияСCARGORUN.ОпределитьТипПоСсылкеНаОбъект(СсылкаНаОбъект);
КонецФункции

Процедура ИнициализироватьДополнительныеСведения() Экспорт
	УстановитьПривилегированныйРежим(Истина);

	cgr_ИнтеграцияСCARGORUNПереопределяемый.ИнициализироватьДополнительныеСведения();
КонецПроцедуры